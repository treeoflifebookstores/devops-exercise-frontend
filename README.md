# devops-exercise-frontend

## Local System Prerequisites
- [Install NVM](https://github.com/nvm-sh/nvm)
- [Install Yarn](https://yarnpkg.com/getting-started/migration#why-should-you-migrate)
  - `npm install -g yarn`

## Project setup (in project root)
```
nvm install
nvm use
yarn install
cp .env.template .env
# Update 'VUE_APP_API_BASE_URL' in '.env' file to change connection to backend
```

### Compiles and hot-reloads for local development/testing
```
yarn serve
```

### Compiles and minifies for production (Creates static HTML/CSS/JS code for production in './dist' folder)
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
